#include "main.h"
#include "stm32f1xx_it.h"

volatile uint8_t exti_signal;
volatile uint16_t time_out;


void NMI_Handler(void)
{
	while (1)
	{

	}
}


void HardFault_Handler(void)
{
	while (1)
	{

	}
}


void MemManage_Handler(void)
{
	while (1)
	{

	}
}


void BusFault_Handler(void)
{
	while (1)
	{

	}
}


void UsageFault_Handler(void)
{
	while (1)
	{

	}
}


void SVC_Handler(void)
{

}


void DebugMon_Handler(void)
{

}


void PendSV_Handler(void)
{

}


void SysTick_Handler(void)
{
	if (time_out != 0) time_out--;
}


void EXTI9_5_IRQHandler(void)
{
	if (LL_EXTI_IsActiveFlag_0_31(LL_EXTI_LINE_6) != RESET)
	{
		LL_EXTI_ClearFlag_0_31(LL_EXTI_LINE_6);
		time_out = 1000; 										//0.5 second
		exti_signal = 1;									//signal to indicate detection of rotation
	}
}


/*
void USART1_IRQHandler(void)
{

}
 */

