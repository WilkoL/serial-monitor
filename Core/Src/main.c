
#include "main.h"


void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_TIM3_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_ADC1_Init(void);


uint32_t baudrate_table[10] =
{
		2400,
		4800,
		9600,
		14400,
		19200,
		38400,
		57600,
		115200,
		128000,
		256000
};

extern volatile uint8_t exti_signal;
extern volatile uint16_t time_out;


int main(void)
{
	char buffer[30];
	uint16_t adc_ch0;
	uint16_t v_battery;

	uint8_t line = 0;						//screen has 9 lines available for text (0..8)
	uint8_t column = 0;						//every line is 28 text characters long (0..27)
	char new_char = '\0';
	uint16_t x = 0;
	uint16_t y = 0;

	uint8_t baudrate_selection = 5;			//38400

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_AFIO);
	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_PWR);

	NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4);
	LL_GPIO_AF_Remap_SWJ_NOJTAG();

	SystemClock_Config();
	MX_GPIO_Init();
	MX_TIM3_Init();
	MX_ADC1_Init();
	MX_USART1_UART_Init();

	LL_ADC_Enable(ADC1);									//to measure battery voltage
	LL_ADC_StartCalibration(ADC1);
	while (LL_ADC_IsCalibrationOnGoing(ADC1));
	LL_mDelay(10);

	LL_ADC_REG_StartConversionSWStart(ADC1);
	while (LL_ADC_IsActiveFlag_EOS(ADC1) == RESET);
	adc_ch0 = LL_ADC_REG_ReadConversionData12(ADC1);
	v_battery = ((uint32_t) adc_ch0 * 105) / 100;

	lcd_config();											//init ili9341
	lcd_fillscreen(DARKBLUE);
	lcd_writestring(10,  10, "Battery Voltage", Font_11x18, WHITE, DARKBLUE);

	sprintf(buffer, "%d", v_battery);
	lcd_writestring(10,  45, buffer, Font_11x18, WHITE, DARKBLUE);
	lcd_writestring(65,  45, "mV", Font_11x18, WHITE, DARKBLUE);
	LL_mDelay(1999);

	lcd_fillscreen(DARKBLUE);

	lcd_writestring(10,  10,  "_____ASCII terminal_____",Font_11x18, WHITE, DARKBLUE);
	lcd_writestring(10,  60,  " 2400    19200   128000" ,Font_11x18, WHITE, DARKBLUE);
	lcd_writestring(10,  85,  " 4800   *38400   256000" ,Font_11x18, WHITE, DARKBLUE);
	lcd_writestring(10,  110, " 9600    57600"          ,Font_11x18, WHITE, DARKBLUE);
	lcd_writestring(10,  135, "14400   115200   bps"    ,Font_11x18, WHITE, DARKBLUE);
	lcd_writestring(10,  185, "8 bits 1 stop no parity" ,Font_11x18, WHITE, DARKBLUE);




	//LL_mDelay(4999);

	//lcd_fillscreen(DARKBLUE);

	line = 8;
	column = 0;

	USART1_init();											//init usart
	//														//reconfigure to 38400
	LL_USART_SetBaudRate(USART1, 96000000UL, baudrate_table[baudrate_selection]);

	LL_TIM_EnableCounter(TIM3);								//rotary encoder ---> baudrate
	LL_SYSTICK_EnableIT();									//for time_out counter
	LL_EXTI_EnableIT_0_31(LL_EXTI_LINE_6);					//enable detection of rotation

	while (1)
	{
		if (USART1_available())
		{
			new_char = USART1_getc();

			if ((new_char == '\n') || (new_char == '\r'))
			{
				if (column < 27)							//not at the end of the line
				{
					y = (uint16_t) (10 + (line * 25));		//so fill rest of the line with spaces
					for (uint8_t i = column; i <= 27; i++)
					{
						x = (uint16_t) (5 + (i * 11));
						lcd_writechar(x, y, ' ', Font_11x18, WHITE, DARKBLUE);
					}
				}
				column = 0;									//then set cursor (underscore)
				if (line < 8) line++;						//at the beginning of the next line
				else line = 0;

				y = (uint16_t) (10 + (line * 25));
				lcd_writechar(5, y, '_', Font_11x18, WHITE, DARKBLUE);

			}

			if ((new_char > 0x1F) && (new_char < 0x7F))
			{
				x = (uint16_t) (5 + (column * 11));			//show received char
				y = (uint16_t) (10 + (line * 25));
				lcd_writechar(x, y, new_char, Font_11x18, WHITE, DARKBLUE);

				if (column < 27)							//and set cursor after it
				{
					lcd_writechar(x+11, y, '_', Font_11x18, WHITE, DARKBLUE);
					column++;
				}
				else
				{
					column = 0;
					if (line < 8) line++;
					else line = 0;

					//x = (uint16_t) 5;
					y = (uint16_t) (10 + (line * 25));
					lcd_writechar(5, y, '_', Font_11x18, WHITE, DARKBLUE);
				}
			}
		}
		if (exti_signal == 1)
		{
			exti_signal = 0;

			lcd_fillscreen(DARKBLUE);
			lcd_writestring(10,  10, "Baudrate:", Font_11x18, WHITE, DARKBLUE);

			while (time_out)
			{
				baudrate_selection = LL_TIM_GetCounter(TIM3) / 2;
				LL_USART_SetBaudRate(USART1, 96000000UL, baudrate_table[baudrate_selection]);

				sprintf(buffer, "%ld     ", baudrate_table[baudrate_selection]);
				lcd_writestring(10,  45, buffer, Font_11x18, WHITE, DARKBLUE);
			}

			lcd_fillscreen(DARKBLUE);
		}
	}
}


void SystemClock_Config(void)
{
	LL_FLASH_SetLatency(LL_FLASH_LATENCY_2);
	while(LL_FLASH_GetLatency()!= LL_FLASH_LATENCY_2);
	LL_FLASH_EnablePrefetch();
	LL_RCC_HSE_Enable();
	while(LL_RCC_HSE_IsReady() != 1);
	LL_RCC_PLL_ConfigDomain_SYS(LL_RCC_PLLSOURCE_HSE_DIV_1, LL_RCC_PLL_MUL_12);
	LL_RCC_PLL_Enable();
	while(LL_RCC_PLL_IsReady() != 1);
	LL_RCC_SetAHBPrescaler(LL_RCC_SYSCLK_DIV_1);
	LL_RCC_SetAPB1Prescaler(LL_RCC_APB1_DIV_2);
	LL_RCC_SetAPB2Prescaler(LL_RCC_APB2_DIV_1);
	LL_RCC_SetSysClkSource(LL_RCC_SYS_CLKSOURCE_PLL);
	while(LL_RCC_GetSysClkSource() != LL_RCC_SYS_CLKSOURCE_STATUS_PLL);
	LL_Init1msTick(96000000);
	LL_SetSystemCoreClock(96000000);
	LL_RCC_SetADCClockSource(LL_RCC_ADC_CLKSRC_PCLK2_DIV_8);
}


static void MX_ADC1_Init(void)
{
	LL_ADC_InitTypeDef ADC_InitStruct = {0};
	LL_ADC_CommonInitTypeDef ADC_CommonInitStruct = {0};
	LL_ADC_REG_InitTypeDef ADC_REG_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_ADC1);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ANALOG;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	ADC_InitStruct.DataAlignment = LL_ADC_DATA_ALIGN_RIGHT;
	ADC_InitStruct.SequencersScanMode = LL_ADC_SEQ_SCAN_DISABLE;
	LL_ADC_Init(ADC1, &ADC_InitStruct);

	ADC_CommonInitStruct.Multimode = LL_ADC_MULTI_INDEPENDENT;
	LL_ADC_CommonInit(__LL_ADC_COMMON_INSTANCE(ADC1), &ADC_CommonInitStruct);

	ADC_REG_InitStruct.TriggerSource = LL_ADC_REG_TRIG_SOFTWARE;
	ADC_REG_InitStruct.SequencerLength = LL_ADC_REG_SEQ_SCAN_DISABLE;
	ADC_REG_InitStruct.SequencerDiscont = LL_ADC_REG_SEQ_DISCONT_DISABLE;
	ADC_REG_InitStruct.ContinuousMode = LL_ADC_REG_CONV_SINGLE;
	ADC_REG_InitStruct.DMATransfer = LL_ADC_REG_DMA_TRANSFER_NONE;
	LL_ADC_REG_Init(ADC1, &ADC_REG_InitStruct);

	LL_ADC_REG_SetSequencerRanks(ADC1, LL_ADC_REG_RANK_1, LL_ADC_CHANNEL_0);
	LL_ADC_SetChannelSamplingTime(ADC1, LL_ADC_CHANNEL_0, LL_ADC_SAMPLINGTIME_55CYCLES_5);
}


static void MX_TIM3_Init(void)
{
	LL_TIM_InitTypeDef TIM_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_TIM3);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_6|LL_GPIO_PIN_7;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = LL_GPIO_PULL_UP;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	LL_TIM_SetEncoderMode(TIM3, LL_TIM_ENCODERMODE_X2_TI1);

	LL_TIM_IC_SetActiveInput(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_IC_FILTER_FDIV32_N8);
	LL_TIM_IC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH1, LL_TIM_IC_POLARITY_FALLING);

	LL_TIM_IC_SetActiveInput(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_ACTIVEINPUT_DIRECTTI);
	LL_TIM_IC_SetPrescaler(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_ICPSC_DIV1);
	LL_TIM_IC_SetFilter(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_IC_FILTER_FDIV32_N8);
	LL_TIM_IC_SetPolarity(TIM3, LL_TIM_CHANNEL_CH2, LL_TIM_IC_POLARITY_FALLING);

	TIM_InitStruct.Prescaler = 0;
	TIM_InitStruct.CounterMode = LL_TIM_COUNTERMODE_UP;
	TIM_InitStruct.Autoreload = 19;
	TIM_InitStruct.ClockDivision = LL_TIM_CLOCKDIVISION_DIV4;
	LL_TIM_Init(TIM3, &TIM_InitStruct);

	LL_TIM_DisableARRPreload(TIM3);
	LL_TIM_SetTriggerOutput(TIM3, LL_TIM_TRGO_RESET);
	LL_TIM_DisableMasterSlaveMode(TIM3);
}



static void MX_USART1_UART_Init(void)
{
	LL_USART_InitTypeDef USART_InitStruct = {0};
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_9;					//PA9   ------> USART1_TX
	GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_10;					//PA10   ------> USART1_RX
	GPIO_InitStruct.Mode = LL_GPIO_MODE_FLOATING;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	//GPIO_InitStruct.Pin = LL_GPIO_PIN_12;					//PA12   ------> USART1_RTS
	//GPIO_InitStruct.Mode = LL_GPIO_MODE_ALTERNATE;
	//GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	//GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	//LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	NVIC_SetPriority(USART1_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(USART1_IRQn);

	USART_InitStruct.BaudRate = 38400;
	USART_InitStruct.DataWidth = LL_USART_DATAWIDTH_8B;
	USART_InitStruct.StopBits = LL_USART_STOPBITS_1;
	USART_InitStruct.Parity = LL_USART_PARITY_NONE;
	USART_InitStruct.TransferDirection = LL_USART_DIRECTION_TX_RX;
	USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_NONE;
	//USART_InitStruct.HardwareFlowControl = LL_USART_HWCONTROL_RTS;
	USART_InitStruct.OverSampling = LL_USART_OVERSAMPLING_16;
	LL_USART_Init(USART1, &USART_InitStruct);

	LL_USART_ConfigAsyncMode(USART1);
	LL_USART_Enable(USART1);
}



static void MX_GPIO_Init(void)
{
	LL_GPIO_InitTypeDef GPIO_InitStruct = {0};
	LL_EXTI_InitTypeDef EXTI_InitStruct = {0};

	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOA);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOB);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOC);
	LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_GPIOD);


	LL_GPIO_SetOutputPin(GPIOA, LL_GPIO_PIN_1|LL_GPIO_PIN_2|LL_GPIO_PIN_3|LL_GPIO_PIN_4
			|LL_GPIO_PIN_5);

	LL_GPIO_SetOutputPin(GPIOB, LL_GPIO_PIN_0|LL_GPIO_PIN_1|LL_GPIO_PIN_2|LL_GPIO_PIN_10
			|LL_GPIO_PIN_11|LL_GPIO_PIN_12|LL_GPIO_PIN_13|LL_GPIO_PIN_14
			|LL_GPIO_PIN_15|LL_GPIO_PIN_3|LL_GPIO_PIN_4|LL_GPIO_PIN_5
			|LL_GPIO_PIN_6|LL_GPIO_PIN_7|LL_GPIO_PIN_8|LL_GPIO_PIN_9);


	GPIO_InitStruct.Pin = LL_GPIO_PIN_1|LL_GPIO_PIN_2
			|LL_GPIO_PIN_3|LL_GPIO_PIN_4|LL_GPIO_PIN_5;					//controls to ili9341
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = LL_GPIO_PIN_0|LL_GPIO_PIN_1|LL_GPIO_PIN_2		//data to ili9341
			|LL_GPIO_PIN_3|LL_GPIO_PIN_4|LL_GPIO_PIN_5|LL_GPIO_PIN_6
			|LL_GPIO_PIN_7|LL_GPIO_PIN_8|LL_GPIO_PIN_9|LL_GPIO_PIN_10
			|LL_GPIO_PIN_11|LL_GPIO_PIN_12|LL_GPIO_PIN_13|LL_GPIO_PIN_14
			|LL_GPIO_PIN_15;
	GPIO_InitStruct.Mode = LL_GPIO_MODE_OUTPUT;
	GPIO_InitStruct.Speed = LL_GPIO_SPEED_FREQ_HIGH;
	GPIO_InitStruct.OutputType = LL_GPIO_OUTPUT_PUSHPULL;
	LL_GPIO_Init(GPIOB, &GPIO_InitStruct);


	//EXTI on PA6 which is also TIM3_CH1 for TIM3 in Encoder Mode
	LL_GPIO_AF_SetEXTISource(LL_GPIO_AF_EXTI_PORTA, LL_GPIO_AF_EXTI_LINE6);

	EXTI_InitStruct.Line_0_31 = LL_EXTI_LINE_6;							//detect rotation of encoder
	EXTI_InitStruct.LineCommand = ENABLE;
	EXTI_InitStruct.Mode = LL_EXTI_MODE_IT;
	EXTI_InitStruct.Trigger = LL_EXTI_TRIGGER_FALLING;
	LL_EXTI_Init(&EXTI_InitStruct);

	LL_GPIO_SetPinPull(GPIOA, LL_GPIO_PIN_6, LL_GPIO_PULL_UP);
	LL_GPIO_SetPinMode(GPIOA, LL_GPIO_PIN_6, LL_GPIO_MODE_INPUT);

	NVIC_SetPriority(EXTI9_5_IRQn, NVIC_EncodePriority(NVIC_GetPriorityGrouping(),0, 0));
	NVIC_EnableIRQ(EXTI9_5_IRQn);
}


void Error_Handler(void)
{
	__disable_irq();
	while (1)
	{
	}
}

#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t *file, uint32_t line)
{

}
#endif
