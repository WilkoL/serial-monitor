#include <usart1.h>
#include "stm32f1xx.h"
#include "stm32f1xx_ll_usart.h"


#if (USART1_RXBUFF_SIZE & USART1_RXBUFF_MASK)
#error RX BUFFER SIZE is not a power of 2
#endif
#if (USART1_TXBUFF_SIZE & USART1_TX_BUFF_MASK)
#error USART TXBUFF SIZE is not a power of 2
#endif


volatile char	USART1_rxbuff[USART1_RXBUFF_SIZE];
volatile char	USART1_txbuff[USART1_RXBUFF_SIZE];
volatile uint16_t	USART1_rxhead = 0;
volatile uint16_t	USART1_rxtail = 0;
volatile uint16_t	USART1_txhead = 0;
volatile uint16_t	USART1_txtail = 0;
volatile uint16_t 	USART1_RX_ERROR = 0;

void USART1_init()
{
	LL_USART_EnableIT_RXNE(USART1);
}

char USART1_getc(void)
{
	uint16_t tmp;
	char data;

	if (USART1_rxhead == USART1_rxtail)
	{
		return(0); //no data available
	}

	tmp = (USART1_rxtail + 1) & USART1_RXBUFF_MASK;
	data = USART1_rxbuff[tmp];
	USART1_rxtail = tmp;
	return(data);
}


void USART1_putc(char data)
{
	uint16_t tmp;

	tmp = (USART1_txhead + 1) & USART1_TXBUFF_MASK;
	while (tmp == USART1_txtail) {}                            //wait for free space in tx buffer

	USART1_txbuff[tmp] = data;
	USART1_txhead = tmp;

	LL_USART_EnableIT_TXE(USART1);
}


void USART1_puts(char *s)
{
	while (*s) USART1_putc(*s++);
}


uint16_t USART1_available(void)
{
	return((USART1_RXBUFF_MASK + USART1_rxhead - USART1_rxtail) % USART1_RXBUFF_MASK);
}


void USART1_flush(void)
{
	USART1_rxhead = USART1_rxtail;
}


void USART1_IRQHandler(void)
{
	uint16_t tmp;
	char data;

	if (LL_USART_IsActiveFlag_RXNE(USART1))						//data is ontvangen
	{
		USART1_RX_ERROR = 0;
		data = (char) LL_USART_ReceiveData8(USART1) & 0xFF;
		tmp = (USART1_rxhead + 1) & USART1_RXBUFF_MASK;
		if (tmp == USART1_rxtail)
		{
			tmp--;												//buffer is full, simply
			USART1_rxbuff[tmp] = data;							//put received char in
			USART1_rxhead = tmp;								//last position (drop)
		}
		else
		{
			USART1_rxbuff[tmp] = data;
			USART1_rxhead = tmp;
		}
		LL_USART_ClearFlag_RXNE(USART1);         				//data is verwerkt, clear flag
	}



	if (LL_USART_IsActiveFlag_TXE(USART1))             			//data moet worden verzonden
	{
		if (USART1_txhead == USART1_txtail)
		{
			LL_USART_DisableIT_TXE(USART1);  					//tx buffer empty, disable TXE interrupt
		}
		else
		{
			tmp = (USART1_txtail + 1) & USART1_TXBUFF_MASK;
			USART1_txtail = tmp;
			LL_USART_TransmitData8(USART1, USART1_txbuff[tmp]);
		}
		LL_USART_ClearFlag_TC(USART1);          				//data wordt verzonden, clear flag
	}
}
