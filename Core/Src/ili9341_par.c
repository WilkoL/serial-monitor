#include "ili9341_par.h"


#define ILI9341_MADCTL_MY  0x80
#define ILI9341_MADCTL_MX  0x40
#define ILI9341_MADCTL_MV  0x20
#define ILI9341_MADCTL_ML  0x10
#define ILI9341_MADCTL_RGB 0x00
#define ILI9341_MADCTL_BGR 0x08
#define ILI9341_MADCTL_MH  0x04


#define LCD_DATA	(GPIOB)

#define LCD_CONTROL	(GPIOA)
#define LCD_RST		(LL_GPIO_PIN_1)
#define LCD_CS		(LL_GPIO_PIN_2)
#define LCD_RS		(LL_GPIO_PIN_3)
#define LCD_WR_LO	(LL_GPIO_PIN_4)
#define LCD_RD		(LL_GPIO_PIN_5)

#define LCD_WR_HI	((uint32_t) LCD_WR_LO)

// default orientation
/*
#define WIDTH  240
#define HEIGHT 320
#define ROTATION (ILI9341_MADCTL_MX | ILI9341_MADCTL_BGR)
*/

// rotate right
/*
#define WIDTH  320
#define HEIGHT 240
#define ROTATION (ILI9341_MADCTL_MX | ILI9341_MADCTL_MY | ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR)
*/

// rotate left

#define WIDTH  320
#define HEIGHT 240
#define ROTATION (ILI9341_MADCTL_MV | ILI9341_MADCTL_BGR)


// upside down
/*
#define WIDTH  240
#define HEIGHT 320
#define ROTATION (ILI9341_MADCTL_MY | ILI9341_MADCTL_BGR)
*/



/***************************************************************/
//private functions
/***************************************************************/

static void lcd_command_write(uint8_t data);
static void lcd_data_write(uint8_t * buff, uint16_t buff_size);
static void lcd_init(void);
static void lcd_setaddresswindow(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1);


static void lcd_command_write(uint8_t cmd)
{
	LCD_CONTROL->BRR = LCD_RS;					//set RS to command mode

	LCD_DATA->ODR = cmd;
	LCD_CONTROL->BSRR = LCD_WR_HI;
	LCD_CONTROL->BRR = LCD_WR_LO;					//toggle WR
	LCD_CONTROL->BSRR = LCD_WR_HI;

	LCD_CONTROL->BSRR = (uint32_t) LCD_RS;		//set RS back to data mode

}


static void lcd_data_write(uint8_t * buffer, uint16_t buffer_size)
{
	uint16_t temp;

	temp = 0;

	while (temp < buffer_size)
	{
		LCD_DATA->ODR = buffer[temp++];
		LCD_CONTROL->BSRR = LCD_WR_HI;
		LCD_CONTROL->BRR = LCD_WR_LO;
		LCD_CONTROL->BSRR = LCD_WR_HI;
	}
}



static void lcd_init(void)
{
	LL_GPIO_SetOutputPin(LCD_CONTROL, LCD_RST);				//RST high
	LL_mDelay(10);
	LL_GPIO_ResetOutputPin(LCD_CONTROL, LCD_RST);			//RST low
	LL_mDelay(10);
	LL_GPIO_SetOutputPin(LCD_CONTROL, LCD_RST);				//RST high
	LL_mDelay(10);

	LL_GPIO_SetOutputPin(LCD_CONTROL, LCD_CS);				//CS high
	LL_GPIO_SetOutputPin(LCD_CONTROL, LCD_WR_LO);				//WR high
	LL_GPIO_SetOutputPin(LCD_CONTROL, LCD_RD);				//RD high

	LL_GPIO_ResetOutputPin(LCD_CONTROL, LCD_CS);			//CS low

	lcd_command_write(0xF7); 								// Pump ratio control
	{
		uint8_t data[] = { 0x20 };
		lcd_data_write(data, sizeof(data));
	}

	lcd_command_write(0x3A); 								// COLMOD: Pixel Format Set
	{
		uint8_t data[] = { 0x55 };
		lcd_data_write(data, sizeof(data));
	}

	lcd_command_write(0x36); 								// Memory Access Control
															// MY  - Row Address Order (bit7)
															// MX  - Column Address Order
															// MV  - Row / Column Exchange
															// ML  - Vertical Refresh Order
															// BGR - RGB-BGR Order
															// MH  - Horizontal Refresh ORDER(bit2)
	{
		uint8_t data[] = { 0x08 };
		lcd_data_write(data, sizeof(data));
	}


	lcd_command_write(0x36);								// MADCTL
	{
		uint8_t data[] = { ROTATION };
		lcd_data_write(data, sizeof(data));
	}

	lcd_command_write(0x11); 								// Sleep OFF
	lcd_command_write(0x29); 								// Display ON

	LL_mDelay(50);
}


static void lcd_setaddresswindow(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1)
{
	lcd_command_write(0x2A); 								// CASET: column address set
	{
		uint8_t data[] = { (uint8_t) (x0 >> 8), (uint8_t) (x0 & 0xFF), (uint8_t) (x1 >> 8), (uint8_t) (x1 & 0xFF) };
		lcd_data_write(data, sizeof(data));
	}

	lcd_command_write(0x2B); 								// RASET: row address set
	{
		uint8_t data[] = { (uint8_t) (y0 >> 8), (uint8_t) (y0 & 0xFF), (uint8_t) (y1 >> 8), (uint8_t) (y1 & 0xFF) };
		lcd_data_write(data, sizeof(data));
	}

	lcd_command_write(0x2C); 								// RAMWR: write to RAM
}




/***************************************************************/
//public functions
/***************************************************************/

void lcd_config(void)
{
	lcd_init();
}


void lcd_fillrectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color)
{
	uint8_t color_high, color_low;

	color_high = (uint8_t) (color >> 8);
	color_low = (uint8_t) (color & 0xFF);

	if((x >= WIDTH) || (y >= HEIGHT)) return;
	if((x + w - 1) >= WIDTH) w = WIDTH - x;
	if((y + h - 1) >= HEIGHT) h = HEIGHT - y;

	lcd_setaddresswindow(x, y, x+w-1, y+h-1);

	for(y = h; y > 0; y--)
	{
		for(x = w; x > 0; x--)
		{
			LCD_DATA->ODR = color_high;
			LCD_CONTROL->BSRR = LCD_WR_HI;
			LCD_CONTROL->BRR = LCD_WR_LO;						//toggle WR
			LCD_CONTROL->BSRR = LCD_WR_HI;

			LCD_DATA->ODR = color_low;
			LCD_CONTROL->BSRR = LCD_WR_HI;
			LCD_CONTROL->BRR = LCD_WR_LO;
			LCD_CONTROL->BSRR = LCD_WR_HI;
		}
	}
}



void lcd_fillscreen(uint16_t color)
{
	lcd_fillrectangle(0, 0, WIDTH, HEIGHT, color);
}



void lcd_writechar(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor)
{
	uint32_t i, b, j;

	lcd_setaddresswindow(x, y, x+10, y+font.height-1);

	for(i = 0; i < font.height; i++)
	{
		b = font.data[(ch - 32) * font.height + i];
		for(j = 0; j < font.width; j++)
		{
			if((b << j) & 0x8000)
			{
				LCD_DATA->ODR = (uint8_t) (color >> 8);
				LCD_CONTROL->BSRR = LCD_WR_HI;
				LCD_CONTROL->BRR = LCD_WR_LO;
				LCD_CONTROL->BSRR = LCD_WR_HI;

				LCD_DATA->ODR = (uint8_t) (color & 0xFF);
				LCD_CONTROL->BSRR = LCD_WR_HI;
				LCD_CONTROL->BRR = LCD_WR_LO;
				LCD_CONTROL->BSRR = LCD_WR_HI;
			}
			else
			{
				LCD_DATA->ODR = (uint8_t) (bgcolor >> 8);
				LCD_CONTROL->BSRR = LCD_WR_HI;
				LCD_CONTROL->BRR = LCD_WR_LO;
				LCD_CONTROL->BSRR = LCD_WR_HI;

				LCD_DATA->ODR = (uint8_t) (bgcolor & 0xFF);
				LCD_CONTROL->BSRR = LCD_WR_HI;
				LCD_CONTROL->BRR = LCD_WR_LO;
				LCD_CONTROL->BSRR = LCD_WR_HI;
			}
		}
	}
}

void lcd_writestring(uint16_t x, uint16_t y, const char* str, FontDef font, uint16_t color, uint16_t bgcolor)
{
	while(*str)
	{
		if(x + font.width >= WIDTH)
		{
			x = 0;
			y += font.height;
			if(y + font.height >= HEIGHT)
			{
				break;
			}

			if(*str == ' ')
			{
				str++;
				continue;
			}
		}
		lcd_writechar(x, y, *str, font, color, bgcolor);
		x += font.width;
		str++;
	}
}



void lcd_drawpixel(uint16_t x, uint16_t y, uint16_t color)
{
	if((x >= WIDTH) || (y >= HEIGHT))
		return;

	lcd_setaddresswindow(x, y, x+1, y+1);
	{
		uint8_t data[] = { (uint8_t) (color >> 8), (uint8_t) (color & 0xFF) };
		lcd_data_write(data, sizeof(data));
	}
}


void lcd_hline(uint16_t x, uint16_t x2, uint16_t y, uint16_t color)
{
	uint16_t i;
	//size_t z;
	uint8_t color_high = (uint8_t) (color >> 8);
	uint8_t color_low = (uint8_t) (color & 0xFF);
	uint8_t data_buffer[2];

	if((x >= WIDTH) || (x2 >= WIDTH) || (y >= HEIGHT))
		return;

	if (x < x2) lcd_setaddresswindow(x, y, x2, y);
	else lcd_setaddresswindow(x2, y, x, y);

	for(i = (x2 - x); i > 0; i--)
	{
		data_buffer[0] = color_high;
		data_buffer[1] = color_low;
		lcd_data_write(data_buffer, sizeof(data_buffer));
	}
}

void lcd_vline(uint16_t x, uint16_t y1, uint16_t y2, uint16_t color)
{
	uint16_t i;
	//size_t z;
	uint8_t color_high = (uint8_t) (color >> 8);
	uint8_t color_low = (uint8_t) (color & 0xFF);
	uint8_t data_buffer[2];

	if (y1 < y2) lcd_setaddresswindow(x,y1,x,y2);
	else lcd_setaddresswindow(x,y2,x,y1);

	for (i = 0; i <= (y2 - y1); i++)
	{
		data_buffer[0] = color_high;
		data_buffer[1] = color_low;
		lcd_data_write(data_buffer, sizeof(data_buffer));
	}
}


void lcd_line(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color)
{
	int16_t dX = x2-x1;
	int16_t dY = y2-y1;
	int16_t dXsym = (dX > 0) ? 1 : -1;
	int16_t dYsym = (dY > 0) ? 1 : -1;
	int16_t dx2;
	int16_t dy2;
	int16_t di;

	if (dX == 0)
	{
		if (y2>y1) lcd_vline(x1,y1,y2,color); else lcd_vline(x1,y2,y1,color);
		return;
	}
	if (dY == 0)
	{
		if (x2>x1) lcd_hline(x1,x2,y1,color); else lcd_hline(x2,x1,y1,color);
		return;
	}

	dX *= dXsym;
	dY *= dYsym;
	dx2 = dX << 1;
	dy2 = dY << 1;


	if (dX >= dY)
	{
		di = dy2 - dX;
		while (x1 != x2)
		{
			lcd_drawpixel(x1,y1,color);
			x1 += dXsym;
			if (di < 0)
			{
				di += dy2;
			}
			else
			{
				di += dy2 - dx2;
				y1 += dYsym;
			}
		}
	}
	else
	{
		di = dx2 - dY;
		while (y1 != y2)
		{
			lcd_drawpixel(x1,y1,color);
			y1 += dYsym;
			if (di < 0)
			{
				di += dx2;
			}
			else
			{
				di += dx2 - dy2;
				x1 += dXsym;
			}
		}
	}
	lcd_drawpixel(x1,y1,color);
}
