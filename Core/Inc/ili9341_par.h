#ifndef __ILI9341_PAR_H___
#define __ILI9341_PAR_H___


#include <main.h>
#include <stdlib.h>
#include "ili9341_font.h"


#define	BLACK   	0x0000
#define	BLUE   		0x001F
#define	DARKBLUE   	0x000F
#define	RED    		0xF800
#define	GREEN  		0x07E0
#define CYAN   		0x07FF
#define MAGENTA		0xF81F
#define YELLOW 		0xFFE0
#define WHITE  		0xFFFF

void lcd_config(void);
void lcd_fillscreen(uint16_t color);
void lcd_fillrectangle(uint16_t x, uint16_t y, uint16_t w, uint16_t h, uint16_t color);
void lcd_drawpixel(uint16_t x, uint16_t y, uint16_t color);
void lcd_hline(uint16_t x, uint16_t x2, uint16_t y, uint16_t color);
void lcd_vline(uint16_t x, uint16_t y1, uint16_t y2, uint16_t color);
void lcd_line(int16_t x1, int16_t y1, int16_t x2, int16_t y2, uint16_t color);
void lcd_writechar(uint16_t x, uint16_t y, char ch, FontDef font, uint16_t color, uint16_t bgcolor);
void lcd_writestring(uint16_t x, uint16_t y, const char* str, FontDef font, uint16_t color, uint16_t bgcolor);


#endif
