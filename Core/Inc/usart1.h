#ifndef STM32F1XX_USART1_H
#define STM32F1XX_USART1_H


#include "stm32f1xx.h"

#define USART1_RXBUFF_SIZE 4096           //max 65536
#define USART1_TXBUFF_SIZE 64            //max 65536

#define USART1_RXBUFF_MASK	(USART1_RXBUFF_SIZE - 1)
#define USART1_TXBUFF_MASK	(USART1_TXBUFF_SIZE - 1)


void USART1_init(void);
char USART1_getc(void);
void USART1_putc(char);
void USART1_puts(char *s);
uint16_t USART1_available(void);
void USART1_flush(void);



#endif
