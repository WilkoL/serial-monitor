#ifndef __ILI9341_FONT_H__
#define __ILI9341_FONT_H__

#include <stdint.h>

typedef struct
{
    uint8_t width;
    uint8_t height;
    uint16_t *data;
} FontDef;


extern FontDef Font_7x10;
extern FontDef Font_11x18;
extern FontDef Font_16x26;

#endif
