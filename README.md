# Serial Monitor

Serial (ascii) monitor with 9 lines of 28 char. 
Baudrate from 2400 up to 256000 baud. 
Digital 3.3V or 5V input.
Powered by a single Lithium-Ion battery. (3.7V nominal)